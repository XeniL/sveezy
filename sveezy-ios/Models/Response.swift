//
//  Response.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 21/03/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation

struct Response : Decodable {

    let organization : Organization?

    enum ResponseCodingKeys: String, CodingKey {
        case organization = "organization"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ResponseCodingKeys.self)
        organization = try container.decodeIfPresent(Organization.self, forKey: .organization)
    }

}
