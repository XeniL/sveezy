//
//  Place.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 09/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation

class Place {
    var name: String?
    var image: URL?
    var distance: String?
    var category: String?
    var logo: URL?
    
    init(name: String, distance: String, category: String, image: URL, logo: URL) {
        self.name = name
        self.distance = distance
        self.category = category
        self.image = image
        self.logo = logo
    }
}
