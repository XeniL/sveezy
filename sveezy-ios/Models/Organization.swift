//
//  Organization.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 12/03/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation

struct Organization: Decodable {
    let id : Int?
    let logoUrl : String?
    let name : String?
    let descriptionField : String?
    let mainCategory : OrganizationMainCategory?
    let images : [OrganizationImages]?

    enum OrganizationCodingKeys: String, CodingKey {
        case id = "id"
        case logoUrl = "logoUrl"
        case name = "name"
        case descriptionField = "description"
        case mainCategory = "mainCategory"
        case images = "images"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OrganizationCodingKeys.self)

        id = try container.decodeIfPresent(Int.self, forKey: .id)
        logoUrl = try container.decodeIfPresent(String.self, forKey: .logoUrl)
        name = try container.decodeIfPresent(String.self, forKey: .name)
        descriptionField = try container.decodeIfPresent(String.self, forKey: .descriptionField)
        mainCategory = try container.decodeIfPresent(OrganizationMainCategory.self, forKey: .mainCategory)
        images = try container.decodeIfPresent([OrganizationImages].self, forKey: .images)
    }
}

struct OrganizationImages : Decodable {

    let thumbUrl : String?
    let originalUrl : String?

    enum OrganizationImagesCodingKeys: String, CodingKey {
        case thumbUrl = "thumbUrl"
        case originalUrl = "originalUrl"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OrganizationImagesCodingKeys.self)
        thumbUrl = try container.decodeIfPresent(String.self, forKey: .thumbUrl)
        originalUrl = try container.decodeIfPresent(String.self, forKey: .originalUrl)
    }

}

struct OrganizationMainCategory : Decodable {

    let id : Int?
    let name : String?

    enum OrganizationMainCategoryCodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: OrganizationMainCategoryCodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        name = try container.decodeIfPresent(String.self, forKey: .name)
    }
}
