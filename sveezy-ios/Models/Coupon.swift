//
//  Coupon.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 09/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation
import UIKit

class Coupon {
    var name: String?
    var image: URL?
    var description: String?
    var standartPrice: String?
    var price: String?
    var logo: URL?
    
    init(name: String, description: String, standartPrice: String, price: String, image: URL, logo: URL){
        self.name = name
        self.description = description
        self.standartPrice = standartPrice
        self.price = price
        self.image = image
        self.logo = logo
    }
}
