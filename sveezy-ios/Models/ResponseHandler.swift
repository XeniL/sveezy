//
//  RootClass.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 21/03/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation

struct ResponseHandler : Decodable {

    let response : Response?

    enum CodingKeys: String, CodingKey {
        case response = "response"
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        response = try container.decodeIfPresent(Response.self, forKey: .response)

    }
}
