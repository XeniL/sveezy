//
//  NotificationSettingsViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 17/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class NotificationSettingsViewController: UIViewController {
    
    @IBOutlet weak var notificationSwitch: UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func dismissView(_ selector: Any){
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
}
