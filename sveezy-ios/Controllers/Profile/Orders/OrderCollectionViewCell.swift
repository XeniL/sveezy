//
//  OrderCollectionViewCell.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 22/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class OrderCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var couponNameLabel: UILabel!
    @IBOutlet weak var orderNumberLabel: UILabel!
    @IBOutlet weak var priceDifferenceLabel: UILabel!
    @IBOutlet weak var couponIcon: UIImageView!
    
}
