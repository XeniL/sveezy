//
//  OrderViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 22/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class OrderViewController: UIViewController {
    
    @IBOutlet weak var orderCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func dismissView(_ selector: Any){
        _ = navigationController?.popToRootViewController(animated: true)
    }

}

extension OrderViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = orderCollectionView.dequeueReusableCell(withReuseIdentifier: "orderCollectionViewCell", for: indexPath) as? OrderCollectionViewCell
        
        cell!.contentView.layer.cornerRadius = 5.0
        cell!.contentView.layer.borderWidth = 1.0
        cell!.contentView.layer.borderColor = UIColor.clear.cgColor
        cell!.contentView.layer.masksToBounds = true
        
        cell!.layer.shadowColor = UIColor("#8894A2").cgColor
        cell!.layer.shadowOffset = CGSize(width: 0, height: 5)
        cell!.layer.shadowRadius = 5
        cell!.layer.shadowOpacity = 0.45
        cell!.layer.masksToBounds = false
        cell!.layer.shadowPath = UIBezierPath(roundedRect: cell!.bounds, cornerRadius: 5.0).cgPath
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeaderView = orderCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "orderCollectionReusableView", for: indexPath) as! OrderCollectionReusableView
        
        return sectionHeaderView
    }
}
