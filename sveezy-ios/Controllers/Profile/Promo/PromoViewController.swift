//
//  PromoViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 27/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class PromoViewController: UIViewController {
    
    @IBOutlet weak var promoEnterView: UIView!
    @IBOutlet weak var promoShareView: UIView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setCutsomViewStyle(customView: promoEnterView)
        setCutsomViewStyle(customView: promoShareView)
        
    }
    
    func setCutsomViewStyle(customView: UIView){
        customView.layer.shadowOffset = CGSize(width: 0, height: 4)
        customView.layer.masksToBounds = false
        customView.layer.shadowColor = UIColor(red: 0.15, green: 0.15, blue: 0.37, alpha: 0.2).cgColor
        customView.layer.shadowOpacity = 1
        customView.layer.shadowRadius = 24
    }
    
    @IBAction func dismissView(_ selector: Any){
        _ = navigationController?.popToRootViewController(animated: true)
    }

}
