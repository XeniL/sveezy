//
//  ProfileViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 11/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profilePicture: UIImageView?
    @IBOutlet weak var editButtonImageView: UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let editButtonTapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileViewController.editProfile(gestureRecogniser:)) )
        
        editButtonImageView?.addGestureRecognizer(editButtonTapGesture)

    }
    
    @objc func editProfile(gestureRecogniser: UITapGestureRecognizer){
        let storyBoard = UIStoryboard(name: "Main", bundle:nil)
        let profileSettingsViewController = storyBoard.instantiateViewController(withIdentifier: "profileSettingsViewController") as! ProfileSettingsViewController
        self.navigationController?.pushViewController(profileSettingsViewController, animated:true)
       
    }
    

}
