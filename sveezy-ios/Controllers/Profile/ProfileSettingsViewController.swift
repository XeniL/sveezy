//
//  ProfileSettingsViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 12/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class ProfileSettingsViewController: UIViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var maleRadioButton: RadioButtonView!
    @IBOutlet weak var femaleRadioButton: RadioButtonView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
    private var datePicker: UIDatePicker?
    private var currentGender: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        datePicker = UIDatePicker()
        datePicker?.datePickerMode = .date
        datePicker?.locale = Locale(identifier: "ru")
        
        datePicker?.addTarget(self, action: #selector(ProfileSettingsViewController.dateChanged(datePicker:)), for: .valueChanged)
        
        let dateTapGesture = UITapGestureRecognizer(target: self, action: #selector(ProfileSettingsViewController.viewTapped(gestureRecogniser:)) )
        
        view.addGestureRecognizer(dateTapGesture)
        birthdayTextField.inputView = datePicker
        
        // MOCK
        maleRadioButton.changeCurrentState(state: true)
        maleRadioButton.buttonIsEnabled(state: false)
        currentGender = 0
        
        maleRadioButton?.radioButtonTapped = {sender in
            self.currentGender = 0
            self.genderButtonIsPressed(currentGender: self.currentGender!)
        }
        
        femaleRadioButton?.radioButtonTapped = {sender in
            self.currentGender = 1
            self.genderButtonIsPressed(currentGender: self.currentGender!)
        }
    }
    
    func genderButtonIsPressed(currentGender: Int){
        if currentGender == 0 {
            self.maleRadioButton.changeCurrentState(state: true)
            self.maleRadioButton.buttonIsEnabled(state: false)
            self.femaleRadioButton.changeCurrentState(state: false)
            self.femaleRadioButton.buttonIsEnabled(state: true)
        } else if currentGender == 1 {
            self.maleRadioButton.changeCurrentState(state: false)
            self.maleRadioButton.buttonIsEnabled(state: true)
            self.femaleRadioButton.changeCurrentState(state: true)
            self.femaleRadioButton.buttonIsEnabled(state: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    @objc func viewTapped(gestureRecogniser: UITapGestureRecognizer){
        view.endEditing(true)
    }
    
    @objc func dateChanged(datePicker: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        birthdayTextField.text = dateFormatter.string(from: datePicker.date)
    }
    
    func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    @IBAction func dismissView(_ selector: Any){
        _ = navigationController?.popToRootViewController(animated: true)
    }
}
