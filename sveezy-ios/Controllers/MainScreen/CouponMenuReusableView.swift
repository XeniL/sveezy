//
//  CouponMenuReusableView.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 08/03/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class CouponMenuReusableView: UICollectionReusableView {
    @IBOutlet weak var greetingsLabel: UILabel!
    @IBOutlet weak var placeCollectionView: UICollectionView!

    var placesList = [Place]()

    override func awakeFromNib() {
        greetingsLabel.text = "Привет, Никита!"

         placesList.append(Place(name: "Hub cafe", distance: "524", category: "Бургеры", image: URL(string: "https://www.seriousfacts.com/wp-content/uploads/2017/04/40_greece-facts-758x426.jpg")!, logo: URL(string: "https://www.freepnglogos.com/uploads/starbucks-logo-png-picture-8.png")!))

         placesList.append(Place(name: "Hub cafe", distance: "524", category: "Бургеры", image: URL(string: "https://www.seriousfacts.com/wp-content/uploads/2017/04/40_greece-facts-758x426.jpg")!, logo: URL(string: "https://www.freepnglogos.com/uploads/starbucks-logo-png-picture-8.png")!))
        
         placesList.append(Place(name: "Hub cafe", distance: "524", category: "Бургеры", image: URL(string: "https://www.seriousfacts.com/wp-content/uploads/2017/04/40_greece-facts-758x426.jpg")!, logo: URL(string: "https://www.freepnglogos.com/uploads/starbucks-logo-png-picture-8.png")!))

    }

}

extension CouponMenuReusableView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return placesList.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = placeCollectionView.dequeueReusableCell(withReuseIdentifier: "placeCollectionViewCell", for: indexPath) as? PlaceCollectionViewCell

        cell!.contentView.layer.cornerRadius = 14.0
        cell!.contentView.layer.masksToBounds = true

        cell!.layer.shadowColor = UIColor("#8894A2").cgColor
        cell!.layer.shadowOffset = CGSize(width: 0, height: 5)
        cell!.layer.shadowRadius = 5
        cell!.layer.shadowOpacity = 0.45
        cell!.layer.masksToBounds = false
        cell!.layer.shadowPath = UIBezierPath(roundedRect: cell!.bounds, cornerRadius: 14).cgPath

        cell!.placeName.text = placesList[indexPath.row].name
        cell!.placeCategory.text = placesList[indexPath.row].category
        cell!.placeDistance.text = placesList[indexPath.row].distance! + " м"
        cell!.placeImage.kf.setImage(with: placesList[indexPath.row].image)
        cell!.placeLogo.kf.setImage(with: placesList[indexPath.row].logo)

        return cell!

    }

}

