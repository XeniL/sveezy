//
//  CouponCollectionViewCell.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 06/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class CouponCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var couponImage: UIImageView!
    @IBOutlet weak var couponName: UILabel!
    @IBOutlet weak var couponInfo: UILabel!
    @IBOutlet weak var couponStandartPrice: UILabel!
    @IBOutlet weak var couponPrice: UILabel!
    @IBOutlet weak var couponLogo: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        couponLogo.layer.borderWidth = 1.0
        couponLogo.layer.masksToBounds = false
        couponLogo.layer.borderColor = UIColor(white: 1.0, alpha: 0).cgColor
        couponLogo.layer.cornerRadius = couponLogo.frame.size.width / 2
        couponLogo.clipsToBounds = true

    }
}
