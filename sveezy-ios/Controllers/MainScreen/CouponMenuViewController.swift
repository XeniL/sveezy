//
//  CouponMenuViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 05/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import Kingfisher

class CouponMenuViewController: UIViewController {

    @IBOutlet weak var couponCollectionView: UICollectionView!

    var couponList = [Coupon]()

    private let couponRefreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        couponList.append(Coupon(name: "Американский панкейк в подарок", description: "1+1 • Hub cafe • Бургерная", standartPrice: "175", price: "75", image: URL(string: "https://img1.cookjournal.ru/2016/07/29/6yS80j67_p1.jpg")!, logo: URL(string: "https://www.freepnglogos.com/uploads/starbucks-logo-png-picture-8.png")!))

         couponList.append(Coupon(name: "Американский панкейк в подарок", description: "1+1 • Hub cafe • Бургерная", standartPrice: "175", price: "75", image: URL(string: "https://img1.cookjournal.ru/2016/07/29/6yS80j67_p1.jpg")!, logo: URL(string: "https://www.freepnglogos.com/uploads/starbucks-logo-png-picture-8.png")!))
        
         couponList.append(Coupon(name: "Американский панкейк в подарок", description: "1+1 • Hub cafe • Бургерная", standartPrice: "175", price: "75", image: URL(string: "https://img1.cookjournal.ru/2016/07/29/6yS80j67_p1.jpg")!, logo: URL(string: "https://www.freepnglogos.com/uploads/starbucks-logo-png-picture-8.png")!))
        
        initCouponCollectionRefresher()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)

        let defaults = UserDefaults.standard
        if defaults.object(forKey: "isFirstTime") == nil {
            defaults.set("No", forKey:"isFirstTime")
            defaults.synchronize()
            let storyboard = UIStoryboard(name: "Onboarding", bundle: nil)
            if let onboardingViewController = storyboard.instantiateViewController(withIdentifier: "OnboardingViewController") as? OnboardingViewController {
                present(onboardingViewController, animated: true, completion: nil)
            }
        }
    }
    
    private func initCouponCollectionRefresher(){
        var attributes = [NSAttributedString.Key: AnyObject]()
        let customTintColor = UIColor(red:0.99, green:0.16, blue:0.48, alpha:1.0)
        attributes[.foregroundColor] = customTintColor
        
        couponRefreshControl.tintColor = customTintColor
        couponRefreshControl.attributedTitle = NSAttributedString(string: "Fetching Coupon Data ...", attributes: attributes)
        
        couponCollectionView.refreshControl = couponRefreshControl
        couponRefreshControl.addTarget(self, action: #selector(refreshCouponData(_:)), for: .valueChanged)
    }
    
    @objc private func refreshCouponData(_ sender: Any) {
        couponRefreshControl.beginRefreshing()
    }
    
}

extension CouponMenuViewController: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return couponList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = couponCollectionView.dequeueReusableCell(withReuseIdentifier: "couponCollectionViewCell", for: indexPath) as? CouponCollectionViewCell
            
            cell!.contentView.layer.cornerRadius = 14.0
            cell!.contentView.layer.borderWidth = 2.0
            cell!.contentView.layer.borderColor = UIColor.clear.cgColor
            cell!.contentView.layer.masksToBounds = true
            
            cell!.layer.shadowColor = UIColor("#8894A2").cgColor
            cell!.layer.shadowOffset = CGSize(width: 0, height: 5)
            cell!.layer.shadowRadius = 5
            cell!.layer.shadowOpacity = 0.45
            cell!.layer.masksToBounds = false
            cell!.layer.shadowPath = UIBezierPath(roundedRect: cell!.bounds, cornerRadius: 14).cgPath
            
            cell!.couponName.text = couponList[indexPath.row].name
            cell!.couponInfo.text = couponList[indexPath.row].description
            cell!.couponStandartPrice.attributedText = (couponList[indexPath.row].standartPrice! + " ₽").strikeThrough()
            cell!.couponPrice.text = couponList[indexPath.row].price! + " ₽"
            cell!.couponImage.kf.setImage(with: couponList[indexPath.row].image)
            cell!.couponLogo.kf.setImage(with: couponList[indexPath.row].logo)
            
            return cell!
        
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeaderView = couponCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "сouponMenuReusableView", for: indexPath) as! CouponMenuReusableView

        return sectionHeaderView
    }
    
}
