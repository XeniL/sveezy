//
//  PlaceCollectionViewCell.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 05/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class PlaceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var placeImage: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeCategory: UILabel!
    @IBOutlet weak var placeDistance: UILabel!
    @IBOutlet weak var placeLogo: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        placeLogo.layer.masksToBounds = false
        placeLogo.layer.cornerRadius = placeLogo.frame.size.width / 2
        placeLogo.clipsToBounds = true
    }

}
