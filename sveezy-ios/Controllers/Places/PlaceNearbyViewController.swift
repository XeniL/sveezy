//
//  PlaceNearbyViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 21/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class PlaceNearbyViewController: UIViewController {
    
    @IBOutlet weak var nearbyPlaceCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
    }
    
    @IBAction func dismissView(_ selector: Any){
        _ = navigationController?.popToRootViewController(animated: true)
    }

}

extension PlaceNearbyViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = nearbyPlaceCollectionView.dequeueReusableCell(withReuseIdentifier: "placeNearbyCollectionViewCell", for: indexPath) as? PlaceNearbyCollectionViewCell
        
        
        cell!.contentView.layer.cornerRadius = 5.0
        cell!.contentView.layer.borderWidth = 1.0
        cell!.contentView.layer.borderColor = UIColor.clear.cgColor
        cell!.contentView.layer.masksToBounds = true
        
        cell!.layer.shadowColor = UIColor("#8894A2").cgColor
        cell!.layer.shadowOffset = CGSize(width: 0, height: 5)
        cell!.layer.shadowRadius = 5
        cell!.layer.shadowOpacity = 0.45
        cell!.layer.masksToBounds = false
        cell!.layer.shadowPath = UIBezierPath(roundedRect: cell!.bounds, cornerRadius: 5.0).cgPath
        
        return cell!
    }
}

