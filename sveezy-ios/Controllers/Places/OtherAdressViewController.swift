//
//  OtherAdressViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 17/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift

class OtherAdressViewController: UIViewController {
    
    @IBOutlet weak var otherAdressCollectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func dismissView(_ selector: Any){
        _ = navigationController?.popToRootViewController(animated: true)
    }
    
}

extension OtherAdressViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = otherAdressCollectionView.dequeueReusableCell(withReuseIdentifier: "otherAdressCollectionViewCell", for: indexPath) as? OtherAdressCollectionViewCell
        
        if indexPath.row == 0 {
            cell!.indicatorView.backgroundColor = UIColor("#FF5864")
            cell!.distanceLabel.textColor = UIColor("#FF5864")
            cell!.pinIconImageView.image = UIImage(named: "activePlaceMarker")
        }
        
        cell!.contentView.layer.cornerRadius = 5.0
        cell!.contentView.layer.borderWidth = 1.0
        cell!.contentView.layer.borderColor = UIColor.clear.cgColor
        cell!.contentView.layer.masksToBounds = true
        
        cell!.layer.shadowColor = UIColor("#8894A2").cgColor
        cell!.layer.shadowOffset = CGSize(width: 0, height: 5)
        cell!.layer.shadowRadius = 5
        cell!.layer.shadowOpacity = 0.45
        cell!.layer.masksToBounds = false
        cell!.layer.shadowPath = UIBezierPath(roundedRect: cell!.bounds, cornerRadius: 5.0).cgPath
        
        return cell!
    }
}
