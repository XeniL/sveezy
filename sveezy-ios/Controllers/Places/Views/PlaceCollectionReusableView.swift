//
//  PlaceCollectionReusableView.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 27/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class PlaceCollectionReusableView: UICollectionReusableView {
    @IBOutlet weak var otherAdressButton: UIStackView!
    @IBOutlet weak var placeAdressLabel: UILabel!
    @IBOutlet weak var placeWorkingHoursLabel: UILabel!
    @IBOutlet weak var placePhoneNumber: UILabel!
}
