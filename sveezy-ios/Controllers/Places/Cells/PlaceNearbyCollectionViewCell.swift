//
//  PlaceNearbyCollectionViewCell.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 21/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class PlaceNearbyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var placeNameLabel: UILabel!
    @IBOutlet weak var streetAdressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var placeLogo: UIImageView!

    
}
