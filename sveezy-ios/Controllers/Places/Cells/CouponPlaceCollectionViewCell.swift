//
//  CouponPlaceCollectionViewCell.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 17/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class CouponPlaceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var couponNameLabel: UILabel!
    @IBOutlet weak var couponPriceLabel: UILabel!
    @IBOutlet weak var couponAvailabilityLabel: UILabel!
    
}
