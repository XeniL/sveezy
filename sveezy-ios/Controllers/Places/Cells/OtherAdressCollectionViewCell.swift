//
//  OtherAdressCollectionViewCell.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 17/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class OtherAdressCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var indicatorView: UIView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var pinIconImageView: UIImageView!
    @IBOutlet weak var streetAdressLabel: UILabel!
    @IBOutlet weak var workingHoursLabel: UILabel!
    @IBOutlet weak var telephoneNumberLabel: UILabel!
    
}
