//
//  PlaceViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 17/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class PlaceViewController: UIViewController {
    
    @IBOutlet weak var couponPlaceCollectionView: UICollectionView!
    var organization1: Organization?


    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func showOtherAdress(_ selector: Any){
//        let storyBoard = UIStoryboard(name: "Main", bundle:nil)
//        let profileSettingsViewController = storyBoard.instantiateViewController(withIdentifier: "otherAdressViewController") as! OtherAdressViewController
//        self.navigationController?.pushViewController(profileSettingsViewController, animated:true)

        NetworkManager.shared.getOrganizationByID(orgId: 1, latitude: 0, longtitude: 0)
    }
    
    @IBAction func dismissView(_ selector: Any){
        self.dismiss(animated: true, completion: nil)
    }
   
}

extension PlaceViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = couponPlaceCollectionView.dequeueReusableCell(withReuseIdentifier: "couponPlaceCollectionViewCell", for: indexPath) as? CouponPlaceCollectionViewCell
        
        cell!.contentView.layer.cornerRadius = 5.0
        cell!.contentView.layer.borderWidth = 1.0
        cell!.contentView.layer.borderColor = UIColor.clear.cgColor
        cell!.contentView.layer.masksToBounds = true
        
        cell!.layer.shadowColor = UIColor("#8894A2").cgColor
        cell!.layer.shadowOffset = CGSize(width: 0, height: 5)
        cell!.layer.shadowRadius = 5
        cell!.layer.shadowOpacity = 0.45
        cell!.layer.masksToBounds = false
        cell!.layer.shadowPath = UIBezierPath(roundedRect: cell!.bounds, cornerRadius: 5.0).cgPath
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let sectionHeaderView = couponPlaceCollectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "placeCollectionReusableView", for: indexPath) as! PlaceCollectionReusableView

        return sectionHeaderView
    }
}
