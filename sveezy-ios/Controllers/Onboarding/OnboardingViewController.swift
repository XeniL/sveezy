//
//  OnboardingViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 22/12/2018.
//  Copyright © 2018 Nikita Elizarov. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController, OnboardingPageViewControllerDelegate {
    
    //MARK: - Outlets
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var proceedButton: UIButton!
    
    //MARK: - Properties
    
    var onboardingPageViewController: OnboardingPageViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    func updateUI(){
        if let index = onboardingPageViewController?.currentIndex {
            pageControl.currentPage = index
        }
    }
    
    func didUpdatePageIndex(currentIndex: Int) {
        updateUI()
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination
        if let pageViewController = destination as? OnboardingPageViewController {
            onboardingPageViewController = pageViewController
            onboardingPageViewController?.onboardingDelegate = self
        }
    }
    
    //MARK: - Actions
    
    @IBAction func proceedToMainScreen(){
        dismiss(animated: true, completion: nil)
    }
}
