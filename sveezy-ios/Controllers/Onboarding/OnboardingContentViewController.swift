//
//  OnboardingContentViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 22/12/2018.
//  Copyright © 2018 Nikita Elizarov. All rights reserved.
//

import UIKit

class OnboardingContentViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var headingLabel: UILabel?
    @IBOutlet weak var subHeadingLabel: UILabel?
    @IBOutlet weak var image: UIImageView?
    
    //MARK: - Properties
    
    var index = 0
    var heading = ""
    var subHeading = ""
    var imageFile = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        headingLabel?.text = heading
        subHeadingLabel?.text = subHeading
        image?.image = UIImage(named: imageFile)
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: (headingLabel?.text)!)
        
        attributedString.setColorForText(textForAttribute: "купонам", withColor: UIColor.red)
        attributedString.setColorForText(textForAttribute: "купоном", withColor: UIColor.red)
        attributedString.setColorForText(textForAttribute: "собой", withColor: UIColor.red)
        
        headingLabel?.attributedText = attributedString

    }
    
}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}
