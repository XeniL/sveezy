//
//  CouponViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 17/01/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import UIKit

class CouponViewController: UIViewController {
    
    @IBOutlet weak var couponLogo: UIImageView!
    @IBOutlet weak var couponNameLabel: UILabel!
    @IBOutlet weak var couponDescriptionLabel: UILabel!
    @IBOutlet weak var couponNumberLabel: UILabel!
    @IBOutlet weak var couponQRImageView: UIImageView!
    @IBOutlet weak var couponAvailabilityLabel: UILabel!
    @IBOutlet weak var couponPriceLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func dismissView(_ selector: Any){
        self.dismiss(animated: true, completion: nil)
    }

}
