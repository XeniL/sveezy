//
//  ConfirmRegisterViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 26/12/2018.
//  Copyright © 2018 Nikita Elizarov. All rights reserved.
//

import UIKit
import PinCodeTextField
import NVActivityIndicatorView
import FirebaseAuth

class ConfirmRegisterViewController: UIViewController {
    
    @IBOutlet weak var pinCodeTextField: PinCodeTextField!
    @IBOutlet weak var resendCodeButton: UIButton?
    @IBOutlet weak var buttonLabel: UILabel?
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView?
    @IBOutlet weak var phoneNumberLabel: UILabel?
    @IBOutlet weak var buttonImage: UIImageView?
    
    private var seconds = 60
    private var timer = Timer()
    private var timeIsRunning:Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        pinCodeTextField.delegate = self
        setUpUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        runTimer()
    }

    func setUpUI(){
        let defaults = UserDefaults.standard
        let phoneNumber = defaults.string(forKey: "phoneNumber") as! String
        phoneNumberLabel?.text = "Мы отправили код на номер " + phoneNumber
        activityIndicator?.type = .ballScaleMultiple
        pinCodeTextField.keyboardType = .numberPad
        hideKeyboardWhenTappedAround()
    }
    
    //MARK: - Timer
    
    func runTimer() {
        resendCodeButton?.isEnabled = false
        buttonImage?.image = UIImage(named: "ButtonInactive")
        timeIsRunning = true
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(ConfirmRegisterViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            timeIsRunning = false
            resendCodeButton?.isEnabled = true
            buttonImage?.image = UIImage(named: "Button")
            buttonLabel?.text = "Прислать еще один код"
        } else {
            seconds -= 1
            buttonLabel!.text = timeString(time: TimeInterval(seconds))
        }
        
    }
    
    func timeString(time:TimeInterval) -> String {
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }

    //MARK: - PinTextField

    func resetPinTextField() {
        self.pinCodeTextField.isUserInteractionEnabled = true
        self.pinCodeTextField.text = ""

        pinCodeTextField.becomeFirstResponder()
    }

    //MARK: - Registration methods

    func failedToConfirmPhoneNumber(error: String){
        if !timeIsRunning {
            resendCodeButton?.isEnabled = false
        }

        resetPinTextField()
        showNotification(title: "Ошибка", message: error)
        activityIndicator?.stopAnimating()
    }

    func failedToSendDataToServer(error: String){
        if !timeIsRunning {
            resendCodeButton?.isEnabled = false
        }

        resetPinTextField()
        showNotification(title: "Ошибка", message: error)
        activityIndicator?.stopAnimating()

    }

    func confirmPhoneWithSms(credentials: PhoneAuthCredential, verificationCode: String) {
        AuthentificationHandler.shared.confirmPhoneNumber(credentials: credentials, verificationCode: verificationCode, completion: sendCredentialsOnServer, failure: failedToConfirmPhoneNumber)
    }

    func sendCredentialsOnServer(){
        let defaults = UserDefaults.standard
        NetworkManager.shared.setAuthorization(phoneNumber: defaults.string(forKey: "phoneNumber")!, completion: self.moveToMainMenuView, failure: failedToSendDataToServer)
    }

    //MARK: - Navigation
    func moveToMainMenuView(){
        if let tabbar = (self.storyboard!.instantiateViewController(withIdentifier: "couponTabBar") as? UITabBarController) {
            self.activityIndicator?.stopAnimating()
            self.present(tabbar, animated: true, completion: nil)
        }
    }

    //MARK: - Resend Code
    
    private func sendAdditionalCodeByPhoneNumber(phoneNumber: String){
        AuthentificationHandler.shared.authentificatePhoneNumber(phoneNumber: phoneNumber, completion: runTimer, failure: failedToConfirmPhoneNumber)
    }


    //MARK: - Actions

    @IBAction func sendAnotherCode(_ sender: Any){
        let defaults = UserDefaults.standard
        let phoneNumber = defaults.string(forKey: "phoneNumber")!

        self.sendAdditionalCodeByPhoneNumber(phoneNumber: phoneNumber)
        seconds = 60
        runTimer()
    }

    @IBAction func returnToPhoneConfirmation(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ConfirmRegisterViewController: PinCodeTextFieldDelegate {

    func textFieldDidEndEditing(_ textField: PinCodeTextField) {
        if pinCodeTextField.text?.count == 6 {
        activityIndicator?.startAnimating()

        resendCodeButton?.isEnabled = true
        let smsCode = pinCodeTextField.text!
        let defaults = UserDefaults.standard
        let credentials: PhoneAuthCredential = PhoneAuthProvider.provider().credential(withVerificationID: defaults.string(forKey: "verificationID")!, verificationCode: pinCodeTextField.text!)

        confirmPhoneWithSms(credentials: credentials, verificationCode: smsCode)

        }
    }

}

