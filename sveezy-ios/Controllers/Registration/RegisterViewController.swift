//
//  RegisterViewController.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 22/12/2018.
//  Copyright © 2018 Nikita Elizarov. All rights reserved.
//

import UIKit
import SwiftPhoneNumberFormatter
import NVActivityIndicatorView
import PhoneNumberKit
import FirebaseAuth

class RegisterViewController: UIViewController {

    @IBOutlet weak var countryCodeButton: CountryCodeView?
    @IBOutlet weak var radioButton: RadioButtonView?
    @IBOutlet weak var proceedButton: UIButton?
    @IBOutlet weak var buttonImage: UIImageView?
    @IBOutlet weak var phoneNumberTextField: PhoneFormattedTextField?
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView?

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpUI()

        radioButton?.radioButtonTapped = {sender in
            if self.radioButton?.currentState == false {
                self.radioButton?.changeCurrentState(state: true)
            } else {
                 self.radioButton?.changeCurrentState(state: false)
            }

            self.checkPhoneAndRadioButton()
        }
    }

    func setUpUI(){
        hideKeyboardWhenTappedAround()
        activityIndicator?.type = .ballScaleMultiple
        phoneNumberTextField!.config.defaultConfiguration = PhoneFormat(defaultPhoneFormat: "(###) ###-##-##")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    private func checkPhoneAndRadioButton(){
        if phoneNumberTextField!.text?.count == 15 && radioButton?.currentState == true {
            proceedButton?.isEnabled = true
            buttonImage?.image = UIImage(named: "Button")
        } else {
            proceedButton?.isEnabled = false
            buttonImage?.image = UIImage(named: "ButtonInactive")
        }
    }

    private func failedPhoneConfirmation(error: String){
        self.checkPhoneAndRadioButton()
        self.activityIndicator?.stopAnimating()
        self.phoneNumberTextField?.text = ""
        self.proceedButton?.isEnabled = true
        self.showNotification(title: "Ошибка", message: error)
    }

    private func successPhoneConfirmation(){
        self.activityIndicator?.stopAnimating()
        let viewController:ConfirmRegisterViewController = self.storyboard?.instantiateViewController(withIdentifier: "confirmViewController") as! ConfirmRegisterViewController
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }

    private func authentificatePhoneNumber(phoneNumber: String){
        AuthentificationHandler.shared.authentificatePhoneNumber(phoneNumber: phoneNumber, completion: successPhoneConfirmation, failure: failedPhoneConfirmation)
    }

    @IBAction func phoneNumberDidEndEditing(_ sender: Any) {
        checkPhoneAndRadioButton()
    }
    
    @IBAction func phoneNumberEditingChanged(_ sender: Any) {
        if phoneNumberTextField!.text?.count == 15 {
            phoneNumberTextField!.resignFirstResponder()
        }
    }
    
    @IBAction func submitPhoneNumber (sender: Any) {
        activityIndicator?.startAnimating()
        proceedButton?.isEnabled = false
        let phoneNumber = "+7" + (phoneNumberTextField?.text?.digits)!

        authentificatePhoneNumber(phoneNumber: phoneNumber)
    }

}
