//
//  RadioButtonView.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 25/12/2018.
//  Copyright © 2018 Nikita Elizarov. All rights reserved.
//

import UIKit

class RadioButtonView: UIView {
    
    let kCONTENT_XIB_NAME = "RadioButtonView"
    
    @IBOutlet weak var contentView: UIView?
    @IBOutlet weak var buttonImage: UIImageView?
    @IBOutlet weak var button: UIButton?

    var radioButtonTapped: (UIButton) -> Void = {_ in }

    var currentState: Bool?

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        Bundle.main.loadNibNamed(kCONTENT_XIB_NAME, owner: self, options: nil)
        contentView!.fixInView(self)
        currentState = false
    }

    @IBAction private func changeState(sender: Any){
        radioButtonTapped(sender as! UIButton)
    }

    func buttonIsEnabled(state: Bool){
        button?.isEnabled = state
    }

    func changeCurrentState(state: Bool){
        if state == true {
            buttonImage?.image = UIImage(named: "ToggleButtonOn")
            currentState = true
        } else if state == false {
            buttonImage?.image = UIImage(named: "ToggleButtonOff")
            currentState = false
        }
    }

}
