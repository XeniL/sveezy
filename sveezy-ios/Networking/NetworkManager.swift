//
//  NetworkManager.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 04/02/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Alamofire
import Moya

struct NetworkManager: SveezyApiProtocol {
    var provider = MoyaProvider<SveezyAPI>(plugins: [NetworkLoggerPlugin(verbose: true)])
    static let shared = NetworkManager()

    private init() {}

    func setAuthorization(phoneNumber: String, completion: @escaping () -> Void, failure: @escaping (String)-> Void) {
        provider.request(.authentification(phoneNumber)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    let statusCode = moyaResponse.statusCode
                    switch statusCode {
                    case 200:
                        completion()
                        print ("Moya returned 200 value", moyaResponse)
                    case 400:
                        failure("Что-то пошло не так")
                        print("Moya returned 400 value", moyaResponse)
                    default:
                        failure("Что-то пошло не так")
                        print("Moya returned default value", moyaResponse)
                    }
                } catch let error {
                    failure(error.localizedDescription)
                }
                
            case let .failure(error):
                print (error)
                failure(error.localizedDescription)
            }
        }
    }

    func getOrganizationList(latitude: Int, longtitude: Int) {
        provider.request(.getOrganizationList(latitude: latitude, longtitude: longtitude)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    print(moyaResponse)
                    let results = try JSONDecoder().decode(ResponseHandler.self, from: moyaResponse.data)


                } catch let error {
                    print(error.localizedDescription)
                }

            case let .failure(error):
                print (error.localizedDescription)
            }
        }
    }
    

    func getOrganizationByID(orgId: Int, latitude: Int, longtitude: Int) {
        provider.request(.getOrganizationByID(orgId)) { result in
            switch result {
            case let .success(moyaResponse):
                do {
                    print (moyaResponse)
                    let results = try JSONDecoder().decode(ResponseHandler.self, from: moyaResponse.data)
                    
                } catch let error {
                    print(error.localizedDescription)
                }

            case let .failure(error):
                print (error.localizedDescription)
            }
        }
    }
}
