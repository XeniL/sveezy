//
//  SveezyAPI.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 28/12/2018.
//  Copyright © 2018 Nikita Elizarov. All rights reserved.
//

import FirebaseAuth
import Moya

public enum SveezyAPI {
    case authentification(String)
    case getOrganizationByID(Int)
    case getOrganizationList(latitude:Int, longtitude: Int)
}

extension SveezyAPI: TargetType {
    public var sampleData: Data {
        return Data.init()
    }
    
    public var baseURL: URL {
        guard let url = URL(string: "https://sveezy-back.herokuapp.com/api/") else { fatalError("baseURL is not configured")}
        return url
    }
    
    public var path: String {
        switch self {
        case .authentification:
            return "users/getOrCreate"
        case .getOrganizationList:
            return "orgs"
        case .getOrganizationByID(let orgId):
            return "orgs/\(orgId)"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .authentification:
            return .post
        case .getOrganizationList, .getOrganizationByID:
            return .get
        }
    }

    public var parameters: [String: Any] {
        switch self {
        case .authentification(let phoneNumber):
            return ["phoneNumber": phoneNumber]
        case .getOrganizationList(let latitude, let longtitude):
            return ["lat": latitude, "lng": longtitude]
//        case .getOrganizationByID(let latitude, let longtitude):
//            return ["lat": latitude, "lng": longtitude]
        default: return [String: Any]()
        }
    }

    public var parameterEncoding: ParameterEncoding {
        return URLEncoding.default
    }
    
    public var task: Task {
        switch self {
        default: return .requestParameters(parameters: parameters, encoding: parameterEncoding)
        }
    }
    
    public var headers: [String : String]? {
        switch self {
        default:
            let defaults = UserDefaults.standard
            AuthentificationHandler.shared.refreshToken(forced: false)

            return [
                "Authorization": "Bearer \(defaults.value(forKey: "token") as! String)",
                "Content-Type": "application/json",
                "Api-Version": "1.0"
            ]
        }
    }
}
