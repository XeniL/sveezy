//
//  AuthentificationHandler.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 08/03/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Foundation
import FirebaseAuth

struct AuthentificationHandler {

    static let shared = AuthentificationHandler()

    private init(){}

    public func authentificatePhoneNumber(phoneNumber: String, completion: @escaping () -> Void, failure: @escaping (String)-> Void){

        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
            if let error = error {
                print("Failed to send phone number", error)
                failure(error.localizedDescription)
                return
            }

            let defaults = UserDefaults.standard
            defaults.set(verificationID, forKey: "verificationID")
            defaults.set(phoneNumber, forKey: "phoneNumber")

            completion()
        }
    }

    public func confirmPhoneNumber(credentials: PhoneAuthCredential, verificationCode: String, completion: @escaping () -> Void, failure: @escaping (String)-> Void){

        Auth.auth().signInAndRetrieveData(with: credentials, completion: { (user, error) in
            if let error = error {
                print ("Failed to create google account", error)
                failure(error.localizedDescription)
                return
            }

            self.refreshToken(forced: true)
            completion()
        })
    }

    public func refreshToken(forced: Bool){
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(forced) { idToken, error in
            if let error = error {
                print ("failed to fetch token", error.localizedDescription)
                return
            }
            let defaults = UserDefaults.standard
            let token = idToken!
            defaults.set(token, forKey: "token")
        }
    }
}
