//
//  SveezyApiProtocol.swift
//  sveezy-ios
//
//  Created by Nikita Elizarov on 12/03/2019.
//  Copyright © 2019 Nikita Elizarov. All rights reserved.
//

import Moya

protocol SveezyApiProtocol {
    var provider: MoyaProvider<SveezyAPI> { get }
    func setAuthorization(phoneNumber: String, completion: @escaping () -> Void, failure: @escaping (String)-> Void)
    func getOrganizationByID(orgId: Int, latitude: Int, longtitude: Int)
    func getOrganizationList(latitude: Int, longtitude: Int)
}
